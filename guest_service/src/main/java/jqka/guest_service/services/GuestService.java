package jqka.guest_service.services;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jqka.guest_service.entities.Guest;
import jqka.guest_service.repository.GuestRepository;


@Service
public class GuestService {
	@Autowired
	private GuestRepository repository;
	
	public List<Guest> getAll() {
		return repository.findAll();
	}
	
	public Guest findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new RuntimeException("Guest is not exist!"));
	}
	
	public Guest findByEmail(String email) {
		return repository.findByEmail(email);
	}

	public Guest save(Guest guest) {
		return repository.save(guest);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}
}
