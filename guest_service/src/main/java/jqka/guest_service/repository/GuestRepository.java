package jqka.guest_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jqka.guest_service.entities.Guest;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long> {
	Guest findByEmail(String email);

}
