package jqka.guest_service.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import jqka.guest_service.dto.BookingDTO;
import jqka.guest_service.dto.GuestDTO;
import jqka.guest_service.entities.Guest;
import jqka.guest_service.services.GuestService;

@RestController
@RequestMapping("api/guest/")
public class GuestController {
	private RestTemplate res;
	@Autowired
	public GuestController(RestTemplate res) {
		super();
		this.res = res;
	}
	
	@Autowired
	private GuestService service;

	@GetMapping("")
	public List<Guest> getAll() {
		return service.getAll();
	}

	@GetMapping("id={id}")
	public ResponseEntity<Guest> getById(@PathVariable("id") Long id) {
		Guest guest = service.findById(id);
		if (guest == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(guest);
	}

	@GetMapping("email={email}")
	public ResponseEntity<Guest> getByEmail(@PathVariable("email") String email) {
		Guest guest = service.findByEmail(email);
		if (guest == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(guest);
	}

	@PostMapping("save")
	public Guest save(@RequestBody Guest guest) {
		Guest g = service.save(guest);
		return g;
	}
	
	@PutMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, @RequestBody Guest guest) {
		Guest existingGuest = service.findById(id);
		if (existingGuest == null) {
			return "not found!";
		}

		existingGuest.setFirstName(guest.getFirstName());
		existingGuest.setLastName(guest.getLastName());
		existingGuest.setEmail(guest.getEmail());
		existingGuest.setAddress(guest.getAddress());

		Guest updatedGuest = service.save(existingGuest);

		return "success: " + updatedGuest;
	}

	@DeleteMapping("delete/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}
	
	@GetMapping("getListBookingByGuestId/{id}")
	public GuestDTO getListBookingByGuestId(@PathVariable Long id) {
	    Guest guest = service.findById(id);
	    Long idB = guest.getGuestId();
	    BookingDTO[] bookingDTOArray = res.getForObject("http://reservation-service:8084/api/reservation/guest={idB}", BookingDTO[].class, idB);
	    List<BookingDTO> bookingDTOList = Arrays.asList(bookingDTOArray);
	    
	    GuestDTO guestDTO = new GuestDTO();
	    guestDTO.setGuestId(guest.getGuestId());
	    guestDTO.setFirstName(guest.getFirstName());
	    guestDTO.setLastName(guest.getLastName());
	    guestDTO.setEmail(guest.getEmail());
	    guestDTO.setAddress(guest.getAddress());
	    guestDTO.setBookingDTO(bookingDTOList);  // Thiết lập danh sách các đối tượng BookingDTO
	    
	    return guestDTO;
	}


}
