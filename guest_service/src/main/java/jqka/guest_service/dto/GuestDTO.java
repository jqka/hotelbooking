package jqka.guest_service.dto;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.AllArgsConstructor;
@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GuestDTO {
	private Long guestId;
	private String firstName;
	private String lastName;
	private String email;
	private String address;
	private List<BookingDTO> bookingDTO;
}
