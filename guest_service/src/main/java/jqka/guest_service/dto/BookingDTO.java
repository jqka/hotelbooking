package jqka.guest_service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {
	private Long reservationId;
	private String guest;
	private String room;
	private Double totalPrice;
	private Date checkInDate;
	private Date checkOutDate;
	private String status;
}
