package jqka.search_service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jqka.search_service.services.SearchRestTemplate;

@RestController
@RequestMapping("api/search/")
public class SearchController {
	@Autowired
	private SearchRestTemplate rest;
	
	@GetMapping("")
	public String hello() {
		return "Hello";
	}

	@GetMapping("hotel")
	public List<Map<String, Object>> allHotel() {
		List<Map<String, Object>> respone = rest.getAllHotel();
		return respone;
	}

	@GetMapping("room")
	public List<Map<String, Object>> allRoom() {
		List<Map<String, Object>> respone = rest.getAllRooms();
		return respone;
	}

	@GetMapping("guest")
	public List<Map<String, Object>> allGuest() {
		List<Map<String, Object>> respone = rest.getAllGuest();
		return respone;
	}

	@GetMapping("reservation")
	public List<Map<String, Object>> allReservation() {
		List<Map<String, Object>> respone = rest.getAllReservation();
		return respone;
	}
}
