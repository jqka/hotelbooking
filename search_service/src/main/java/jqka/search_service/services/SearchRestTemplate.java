package jqka.search_service.services;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class SearchRestTemplate {
	private final String hotelApi = "http://localhost:8081/api/hotel/";
	private final String roomApi = "http://localhost:8082/api/room/";
	private final String guestApi = "http://localhost:8083/api/guest/";
	private final String reservationApi = "http://localhost:8084/api/reservation/";

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;

	public List<Map<String, Object>> getAllHotel() {
		return getCachedData("hotels", hotelApi);
	}

	public List<Map<String, Object>> getAllRooms() {
		return getCachedData("rooms", roomApi);
	}

	public List<Map<String, Object>> getAllGuest() {
		return getCachedData("guests", guestApi);
	}

	public List<Map<String, Object>> getAllReservation() {
		return getCachedData("reservations", reservationApi);
	}

	private List<Map<String, Object>> getCachedData(String key, String apiUrl) {
		// Check if data is already cached
		List<Map<String, Object>> cachedData = (List<Map<String, Object>>) redisTemplate.opsForValue().get(key);
		if (cachedData != null) {
			return cachedData;
		}

		// If not cached, fetch from API and cache it
		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(apiUrl, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Map<String, Object>>>() {
				});
		List<Map<String, Object>> data = responseEntity.getBody();

		// Cache the data with an expiration time (e.g., 1 hour)
		redisTemplate.opsForValue().set(key, data, 1, TimeUnit.HOURS);

		return data;
	}

//	public List<Map<String, Object>> getAllHotel() {
//		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(hotelApi, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<Map<String, Object>>>() {
//				});
//		return responseEntity.getBody();
//	}
//
//	public List<Map<String, Object>> getAllRooms() {
//		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(roomApi, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<Map<String, Object>>>() {
//				});
//		return responseEntity.getBody();
//	}
//
//	public List<Map<String, Object>> getAllGuest() {
//		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(guestApi, HttpMethod.GET, null,
//				new ParameterizedTypeReference<List<Map<String, Object>>>() {
//				});
//		return responseEntity.getBody();
//	}
//
//	public List<Map<String, Object>> getAllReservation() {
//		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(reservationApi, HttpMethod.GET,
//				null, new ParameterizedTypeReference<List<Map<String, Object>>>() {
//				});
//		return responseEntity.getBody();
//	}

}
