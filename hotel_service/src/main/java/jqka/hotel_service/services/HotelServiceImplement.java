package jqka.hotel_service.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jqka.hotel_service.entites.Hotel;
import jqka.hotel_service.repository.HotelRepository;

@Service
public class HotelServiceImplement implements HotelService {
	@Autowired
	private HotelRepository repository;

	@Override
	public List<Hotel> findAll() {
		return repository.findAll();
	}

	@Override
	public Hotel findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new RuntimeException("Hotel is not exist!"));
	}

	@Override
	public Hotel findByName(String name) {
		return repository.findByName(name);
	}

	@Override
	public Hotel save(Hotel hotel) {
		return repository.save(hotel);
	}

	@Override
	public void delete(Long id) {
		repository.deleteById(id);
	}
}
