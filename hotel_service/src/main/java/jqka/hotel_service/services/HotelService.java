package jqka.hotel_service.services;

import java.util.List;

import org.springframework.stereotype.Service;

import jqka.hotel_service.entites.Hotel;

@Service
public interface HotelService {
	List<Hotel> findAll();
	Hotel findById(Long id);
	Hotel findByName(String name);
	Hotel save(Hotel hotel);
	void delete(Long id);

}
