package jqka.hotel_service.services;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HotelRestTemplate {
	private final String urlApi = "http://localhost:8082/api/room/hotel=";

	@Autowired
	private RestTemplate restTemplate;

	public List<Map<String, Object>> getAllRooms() {
		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(urlApi, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Map<String, Object>>>() {
				});
		return responseEntity.getBody();
	}

//	public List<Room> getRoomByHotelId(Long id) {
//		ResponseEntity<List<Map<String, Object>>> responseEntity = restTemplate.exchange(urlApi + id,
//				HttpMethod.GET, null, new ParameterizedTypeReference<List<Map<String, Object>>>() {
//				});
//
//		List<Map<String, Object>> responseBody = responseEntity.getBody();
//
//		List<Room> rooms = new ArrayList<>();
//		for (Map<String, Object> roomMap : responseBody) {
//			Room room = new Room();
//			int rid = (int) roomMap.get("roomId");
//			Long rid2 = (long) rid;
//			room.setRoomId(rid2);
//			room.setHotel((String) roomMap.get("hotel"));
//			room.setRoomNumber((Integer) roomMap.get("roomNumber"));
//			room.setRoomType((String) roomMap.get("roomType"));
//			room.setPricePerNight((Double) roomMap.get("pricePerNight"));
//			room.setStatus((String) roomMap.get("status"));
//			rooms.add(room);
//		}
//		return rooms;
//	}
}
