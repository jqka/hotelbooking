package jqka.hotel_service.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jqka.hotel_service.entites.Hotel;
import jqka.hotel_service.services.HotelRestTemplate;
import jqka.hotel_service.services.HotelService;

@RestController
@RequestMapping("api/hotel/")
public class HotelController {
	@Autowired
	private HotelService service;

	@Autowired
	private HotelRestTemplate restTemplateService;

	@GetMapping("") // All hotel
	public List<Hotel> getAll() {
//		List<Hotel> list = service.findAll();
//		for (Hotel h : list) {
//			List<Room> rooms = restTemplateService.getRoomByHotelId(h.getHotelId());
//			h.setRooms(rooms);
//		}
		return service.findAll();
	}

	@GetMapping("id={hotelId}")
	public ResponseEntity<Hotel> getById(@PathVariable("hotelId") Long hotelId) {
		Hotel hotel = service.findById(hotelId);
		if (hotel == null) {
			return ResponseEntity.notFound().build(); // Return 404 Not Found
		}
//		List<Room> rooms = restTemplateService.getRoomByHotelId(hotelId);
//		hotel.setRooms(rooms);
		return ResponseEntity.ok(hotel); // Return 200 OK with Hotel object
	}

	@GetMapping("name={hotelName}")
	public Hotel getById(@PathVariable("hotelName") String hotelName) {
		Hotel hotel = service.findByName(hotelName);
//		List<Room> rooms = restTemplateService.getRoomByHotelId(hotel.getHotelId());
//		hotel.setRooms(rooms);
		return hotel;
	}

	@PostMapping("save")
	public Hotel save(@RequestBody Hotel hotel) {
		Hotel h = service.save(hotel);
		return h;
	}

	@PutMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, @RequestBody Hotel hotel) {
		Hotel existingHotel = service.findById(id);
		if (existingHotel == null) {
			return "not found!";
		}

		existingHotel.setName(hotel.getName());
		existingHotel.setAddress(hotel.getAddress());
		existingHotel.setEmail(hotel.getEmail());
		existingHotel.setPhone(hotel.getPhone());

		Hotel updatedHotel = service.save(existingHotel);

		return "success: " + updatedHotel;
	}

	@DeleteMapping("delete/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}

	@GetMapping("/room")
	public List<Map<String, Object>> getAllRoom() {
		List<Map<String, Object>> employees = restTemplateService.getAllRooms();
		return employees;
	}

//	@GetMapping("/room/{hotelId}")
//	public List<Room> getRoomByHotelId(@PathVariable("hotelId") Long roomId) {
//		List<Room> rooms = restTemplateService.getRoomByHotelId(roomId);
//
//		return rooms;
//	}

}
