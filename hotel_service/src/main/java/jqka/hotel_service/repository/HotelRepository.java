package jqka.hotel_service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jqka.hotel_service.entites.Hotel;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
	Hotel findByName(String name);
}
