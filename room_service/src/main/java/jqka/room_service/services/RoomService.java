package jqka.room_service.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jqka.room_service.entities.Room;
import jqka.room_service.repository.RoomRepository;

@Service
public class RoomService {
	@Autowired
	private RoomRepository repository;

	public List<Room> getAll() {
		return repository.findAll();
	}
	
	public Room findById(Long id) {
		return repository.findById(id).orElseThrow(() -> new RuntimeException("Room is not exist!"));
	}

	public List<Room> getRoomByHotel(String hotelId) {
		return repository.findByHotel(hotelId);
	}
	
	public List<Room> getRoomByStatus(String status) {
		return repository.findByStatus(status);
	}
	
	public List<Room> getRoomByType(String type) {
		return repository.findByRoomType(type);
	}
	
	public Room save(Room room) {
		return repository.save(room);
	}

	public void delete(Long id) {
		repository.deleteById(id);
	}

}
