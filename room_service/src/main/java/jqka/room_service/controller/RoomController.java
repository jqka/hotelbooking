package jqka.room_service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jqka.room_service.entities.Room;
import jqka.room_service.services.RoomService;

@RestController
@RequestMapping("api/room/")
public class RoomController {
	@Autowired
	private RoomService service;

	@GetMapping("")
	public List<Room> getAll() {
		return service.getAll();
	}

	@GetMapping("id={id}")
	public ResponseEntity<Room> getById(@PathVariable("id") Long id) {
		Room room = service.findById(id);
		if (room == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(room);
	}

	@GetMapping("hotel={hotelId}")
	public List<Room> getRoomByHotel(@PathVariable("hotelId") String hotelId) {
		return service.getRoomByHotel(hotelId);
	}

	@GetMapping("status={status}")
	public List<Room> getRoomByStatus(@PathVariable("status") String status) {
		return service.getRoomByStatus(status);
	}

	@GetMapping("type={type}")
	public List<Room> getRoomByType(@PathVariable("type") String type) {
		return service.getRoomByType(type);
	}

	@PostMapping("save")
	public Room save(@RequestBody Room room) {
		Room respone = service.save(room);
		return respone;
	}

	@PutMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, @RequestBody Room room) {
		Room existingRoom = service.findById(id);
		if (existingRoom == null) {
			return "not found!";
		}

		existingRoom.setHotel(room.getHotel());
		existingRoom.setRoomNumber(room.getRoomNumber());
		existingRoom.setRoomType(room.getRoomType());
		existingRoom.setPricePerNight(room.getPricePerNight());
		existingRoom.setStatus(room.getStatus());

		Room updatedRoom = service.save(existingRoom);

		return "success: " + updatedRoom;
	}

	@DeleteMapping("delete/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}

}
