package jqka.room_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jqka.room_service.entities.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
	  List<Room> findByHotel(String hotel);
	  List<Room> findByStatus(String status);
	  List<Room> findByRoomType(String roomType);
}
