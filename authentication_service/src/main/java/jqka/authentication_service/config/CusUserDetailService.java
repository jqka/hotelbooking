package jqka.authentication_service.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import jqka.authentication_service.entities.Account;
import jqka.authentication_service.repository.AccountRepository;

@Component
public class CusUserDetailService implements UserDetailsService {
	@Autowired
	private AccountRepository accountRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Account> account = accountRepository.finfByUsername(username);
		return account.map(CusUserDetails::new)
				.orElseThrow(() -> new UsernameNotFoundException("Not found username: " + username));
	}

}
