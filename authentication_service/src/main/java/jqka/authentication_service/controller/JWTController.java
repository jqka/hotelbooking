package jqka.authentication_service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.github.bucket4j.Bucket;
import jqka.authentication_service.entities.Account;
import jqka.authentication_service.services.AccountService;

@RestController
@RequestMapping("api/auth")
public class JWTController {
	@Autowired
	private AccountService accountService;

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	private Bucket bucket;

	@PostMapping("/register")
	public String saveAccount(@RequestBody Account account) {
		System.out.println(account);
		return accountService.save(account);
	}

	@PostMapping("/registerAdmin")
	public String saveAdmin(@RequestBody Account account) {
		account.setAdmin(true);
		System.out.println(account);
		return accountService.save(account);
	}

	@PostMapping("/login")
	public String getToken(@RequestBody Account account) {
		if (bucket.tryConsume(1)) {
			Authentication authentication = authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(account.getUsername(), account.getPassword()));
			if (authentication.isAuthenticated()) {
				return accountService.generateToken(account.getUsername());
			} else {
				throw new RuntimeException("invalid access");
			}
		} else {
			return "Rate limit exceeded";
		}

	}

	@GetMapping("/validate")
	public String validateToken(@RequestParam("token") String token) {
		accountService.validateToken(token);
		return "Token is valid";
	}
}
