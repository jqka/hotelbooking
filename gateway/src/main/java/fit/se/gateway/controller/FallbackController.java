package fit.se.gateway.controller;

import org.apache.hc.core5.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {

    @RequestMapping("/fallback/auth-service")
    public ResponseEntity<String> authServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Auth Service is currently unavailable. Please try again later.");
    }

    @RequestMapping("/fallback/hotel-service")
    public ResponseEntity<String> hotelServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Hotel Service is currently unavailable. Please try again later.");
    }

    @RequestMapping("/fallback/room-service")
    public ResponseEntity<String> roomServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Room Service is currently unavailable. Please try again later.");
    }

    @RequestMapping("/fallback/guest-service")
    public ResponseEntity<String> guestServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Guest Service is currently unavailable. Please try again later.");
    }

    @RequestMapping("/fallback/reservation-service")
    public ResponseEntity<String> reservationServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Reservation Service is currently unavailable. Please try again later.");
    }

    @RequestMapping("/fallback/search-service")
    public ResponseEntity<String> searchServiceFallback() {
        return ResponseEntity.status(HttpStatus.SC_SERVICE_UNAVAILABLE)
                .body("Search Service is currently unavailable. Please try again later.");
    }
}
