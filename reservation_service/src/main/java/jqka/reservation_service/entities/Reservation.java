package jqka.reservation_service.entities;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "reservations")
public class Reservation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "reservation_id")
	private Long reservationId;

	@Column(name = "guest_id", nullable = false)
	private String guest;

	@Column(name = "room_id", nullable = false)
	private String room;

	@Column(name = "total_price")
	private Double totalPrice;

	@Column(name = "check_in_date")
	@Temporal(TemporalType.DATE)
	private Date checkInDate;

	@Column(name = "check_out_date")
	@Temporal(TemporalType.DATE)
	private Date checkOutDate;

	private String status;
}