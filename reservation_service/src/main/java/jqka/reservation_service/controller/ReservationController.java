package jqka.reservation_service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jqka.reservation_service.entities.Reservation;
import jqka.reservation_service.services.ReservationService;

@RestController
@RequestMapping("api/reservation/")
public class ReservationController {
	@Autowired
	private ReservationService service;

	@GetMapping("")
	public List<Reservation> getAll() {
		return service.getAll();
	}

	@GetMapping("id={id}")
	public ResponseEntity<Reservation> getById(@PathVariable("id") Long id) {
		Reservation reservation = service.findById(id);
		if (reservation == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(reservation);
	}

	@GetMapping("guest={guest}")
	public ResponseEntity<List<Reservation>> getByGuest(@PathVariable("guest") String id) {
		List<Reservation> reservation = service.findByGuest(id);
		if (reservation == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(reservation);
	}

	@GetMapping("room={room}")
	public ResponseEntity<List<Reservation>> getByRoom(@PathVariable("room") String id) {
		List<Reservation> reservation = service.findByRoom(id);
		if (reservation == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(reservation);
	}

	@PostMapping("save")
	public Reservation save(@RequestBody Reservation reservation) {
		Reservation res = service.save(reservation);
		return res;
	}

	@PutMapping("edit/{id}")
	public String edit(@PathVariable("id") Long id, @RequestBody Reservation reservation) {
		Reservation existingReservation = service.findById(id);
		if (existingReservation == null) {
			return "not found!";
		}

		existingReservation.setGuest(reservation.getGuest());
		existingReservation.setRoom(reservation.getRoom());
		existingReservation.setCheckInDate(reservation.getCheckInDate());
		existingReservation.setCheckOutDate(reservation.getCheckOutDate());
		existingReservation.setTotalPrice(reservation.getTotalPrice());
		existingReservation.setStatus(reservation.getStatus());

		Reservation updatedReservation = service.save(existingReservation);

		return "success: " + updatedReservation;
	}

	@DeleteMapping("delete/{id}")
	public void delete(@PathVariable Long id) {
		service.delete(id);
	}
}
