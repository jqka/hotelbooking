package jqka.reservation_service.repository;

import java.util.List;

import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import jqka.reservation_service.entities.Reservation;

@Repository
public class RedisRepo {
	private HashOperations hashOperations;
    private RedisTemplate redisTemplate;
	public RedisRepo(RedisTemplate redisTemplate) {
        super();
        this.redisTemplate = redisTemplate;
        this.hashOperations = redisTemplate.opsForHash();
    }
    
	public void save(Reservation reservation) {
        hashOperations.put("RESERVATION", reservation.getReservationId(), reservation);
    }

    public List<Reservation> getAllTours() {
        return hashOperations.values("RESERVATION");
    }

    public Reservation getTourById(Long id) {
        return (Reservation) hashOperations.get("RESERVATION", id);
    }

    public void update(Reservation tour) {
        save(tour);
    }

    public void delete(Long id) {
        hashOperations.delete("Reservation", id);
    }
}
