package jqka.reservation_service.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jqka.reservation_service.entities.Reservation;
@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
	List<Reservation> findByGuest(String id);
	List<Reservation> findByRoom(String id);
}
