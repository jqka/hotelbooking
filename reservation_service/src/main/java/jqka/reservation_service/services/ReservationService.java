package jqka.reservation_service.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jqka.reservation_service.entities.Reservation;
import jqka.reservation_service.repository.RedisRepo;
import jqka.reservation_service.repository.ReservationRepository;

@Service
public class ReservationService {
	@Autowired
	private ReservationRepository repository;
	
	@Autowired
	private RedisRepo redisRepo;
	
	public List<Reservation> getAll() {
		redisRepo.getAllTours();
		return repository.findAll();
	}
	
	public Reservation findById(Long id) {
		redisRepo.getTourById(id);
		return repository.findById(id).orElseThrow(() -> new RuntimeException("Reservation is not exist!"));
	}
	
	public List<Reservation> findByGuest(String id) {
		return repository.findByGuest(id);
	}
	
	public List<Reservation> findByRoom(String id) {
		return repository.findByRoom(id);
	}

	public Reservation save(Reservation reservation) {
		Reservation r = repository.save(reservation);
		redisRepo.save(r);
		return r;
	}

	public void delete(Long id) {
		redisRepo.delete(id);
		repository.deleteById(id);
	}
}
